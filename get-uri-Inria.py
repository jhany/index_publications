import sqlite3
from bs4 import BeautifulSoup
import requests
import json
import configparser
from elasticsearch import Elasticsearch

def replace_special_caract(text) :
    text = text.replace("é", "e")
    text = text.replace("è", "e")
    text = text.replace("ê", "e")
    text = text.replace("ë", "e")
    text = text.replace("à", "a")
    text = text.replace("ï", "i")
    text = text.replace("î", "i")
    text = text.replace("ô", "o")
    text = text.replace("ç", "c")
    text = text.replace("ü", "u")
    text = text.replace("ù", "u")
    text = text.replace("-", " ")
    return text

def index_publications(array_pub) :
    i = 0
    for uri in array_pub :
        i = i +1
        id_already = uri.split("/")
        already_done = es.get(index=index_pub, id=id_already[len(id_already) - 1], ignore=[400, 404])
        if already_done["found"] == False :
            r = requests.get(uri+"/tei")
            soup = BeautifulSoup(r.text, "lxml")
            result = {}
            pub = soup.find("biblfull")
            if pub != None :
                # Get authors
                result["authors"] = []
                #Get authors in publication
                authorss = pub.find("analytic").findAll("author")
                for authors in authorss :
                    author = {
                        "forename": getattr(authors.find("forename", {"type" : "first"}), "text", ""),
                        "forename_middle": getattr(authors.find("forename", {"type" : "middle"}), "text", ""),
                        "surname": getattr(authors.find("surname"), "text", ""),
                        "formatName" : replace_special_caract(getattr(authors.find("forename", {"type" : "first"}), "text", "").strip().lower()+ " "+getattr(authors.find("surname"), "text", "").strip().lower()),
                        "halAuthorId": getattr(authors.find("idno", {"type" : "halauthorid"}), "text", ""),
                        "halId": getattr(authors.find("idno", {"type" : "idhal", "notation" : "numeric"}), "text", ""),
                        "halIdRef": getattr(authors.find("idno", {"type" : "IdRef"}), "text", ""),
                        "mail_domain" :  getattr(authors.find("email", {"type" : "domain"}), "text", ""),
                        "mail_md5" :  getattr(authors.find("email", {"type" : "md5"}), "text", ""),
                        "affiliation": authors.find("affiliation")["ref"] if authors.find("affiliation") else  "",
                        "orgName" : authors.find("orgname")["ref"] if authors.find("orgname") else  ""
                    }
                    result["authors"].append(author)
                # Get domains
                result["hal_domains"] = []
                for domain in soup.findAll("classcode", {"scheme": "halDomain"}):
                    hal_domain = {
                        "id": domain["n"],
                        "title": domain.text
                    }
                    result["hal_domains"].append(hal_domain)

                # Get title
                titles = pub.find("analytic").findAll("title")
                result["title_en"] = ""
                result["title_fr"] = ""
                for title in titles :
                    if title.get("xml:lang") == "en":
                        result["title_en"] = title.text
                    else:
                        result["title_fr"] = title.text

                abstracts = pub.find("profiledesc").findAll("abstract") if pub.find("profiledesc") else []

                for abstract in abstracts :
                    # Get abstract
                    if abstract.get("xml:lang") == "en":
                        result["abstract_en"] = abstract.text
                    else:
                        result["abstract_fr"] = abstract.text

                # Get keywords
                result["keywords_en"] = []
                result["keywords_fr"] = []
                keywords = pub.find("profiledesc").findAll("term")
                for word in keywords:
                    if word.get("xml:lang") == "en":
                        result["keywords_en"].append(word.text)
                    else:
                        result["keywords_fr"].append(word.text)

                # Get produced date
                result["year"] = getattr(pub.find('edition').find("date", {"type": "whenProduced"}), "text", "").split('-')[0]

                if result["year"] == "" :
                    result["year"] = getattr(pub.find('edition').find("date", {"type": "whenSubmitted"}), "text", "").split('-')[0]

                # Get hal ref
                result["halId"] = getattr(pub.find('publicationstmt').find("idno", {"type": "halId"}), "text", "")


                # Get pdf url
                result["pdflink"] = pub.find('edition').find("ref", {"type": "file"}).get("target") if pub.find('edition').find("ref", {"type": "file"}) else ""

                # Get publication url
                result["hallink"] = getattr(pub.find('publicationstmt').find("idno", {"type": "halUri"}), "text", "")

                #Get structure in publication
                structures = soup.find("listorg", {"type": "structures"}).findAll('org') if soup.find("listorg", {"type": "structures"}) else []
                result["affiliations"] = []

                for structure in structures :
                    relation_struct_direct = []
                    relation_struct_indirect = []
                    for relation in structure.findAll('relation') :
                        id_struct = relation["active"].replace("#", "")
                        struct_find = soup.find("listorg", {"type": "structures"}).find('org',  {"xml:id" : id_struct})
                        if struct_find != None :
                            if relation["type"] == "direct":
                                direct = {
                                    "type" : struct_find.get("type"),
                                    "id" : struct_find.get("xml:id"),
                                    "status" : struct_find.get("status"),
                                    "name": getattr(struct_find.find("orgname"), "text", ""),
                                    "acronym": getattr(struct_find.find("orgname", {"type": "acronym"}), "text", "").strip().upper(),
                                    "address": getattr(struct_find.find("addrline"), "text", ""),
                                    "country_key": struct_find.find("country")["key"] if struct_find.find("country") else ""
                                }
                                relation_struct_direct.append(direct)
                            else :
                                indirect = {
                                    "type" : struct_find.get("type"),
                                    "id" : struct_find.get("xml:id"),
                                    "status" : struct_find.get("status"),
                                    "name": getattr(struct_find.find("orgname"), "text", ""),
                                    "acronym": getattr(struct_find.find("orgname", {"type": "acronym"}), "text", "").strip().upper(),
                                    "address": getattr(struct_find.find("addrline"), "text", ""),
                                    "country_key": struct_find.find("country")["key"] if struct_find.find("country") else ""
                                }
                                relation_struct_indirect.append(indirect)
                    affiliation = {
                        "type" : structure.get("type"),
                        "id" : structure.get("xml:id"),
                        "status" : structure.get("status"),
                        "name": getattr(structure.find("orgname"), "text", ""),
                        "acronym": getattr(structure.find("orgname", {"type": "acronym"}), "text", "").strip().upper(),
                        "address": getattr(structure.find("addrline"), "text", ""),
                        "country_key": structure.find("country")["key"] if structure.find("country") else "",
                        "url": getattr(structure.find("ref", {"type": "url"}), "text", ""),
                        "date_start" : getattr(structure.find("date", {"type": "start"}), "text", ""),
                        "date_end" : getattr(structure.find("date", {"type": "end"}), "text", ""),
                        "relations" : {
                            "direct" : relation_struct_direct,
                            "indirect" : relation_struct_indirect
                        }
                    }
                    result["affiliations"].append(affiliation)

                #get projet in publication
                projects = soup.find("listorg", {"type": "projects"}).findAll('org') if soup.find("listorg", {"type": "projects"}) else []
                result["projects"] = []
                projet_id = []
                for project in projects :
                    for relation in project.findAll('idno') :
                        projet_id.append(relation.text + " " + relation["type"])
                    project = {
                        "type" : structure.get("type"),
                        "id" : structure.get("xml:id"),
                        "status" : structure.get("status"),
                        "name": getattr(project.find("desc"), "text", ""),
                        "date_start" : getattr(project.find("date", {"type": "start"}), "text", ""),
                        "date_end" : getattr(project.find("date", {"type": "end"}), "text", ""),
                        "multi_id" : projet_id,
                        "acronym": getattr(project.find("orgname"), "text", "").strip().upper(),
                    }
                    result["projects"].append(project)

                result = json.dumps(result)
                es.index(index=index_pub, id=getattr(pub.find('publicationstmt').find("idno", {"type": "halId"}), "text", ""), body=result)

config = configparser.RawConfigParser()
config.read("ConfigFile.properties")
es = Elasticsearch(config.get("elasticsearch", "ip"))
index_pub = config.get("elasticsearch", "index_pub")

con = sqlite3.connect('uri.db')
c = con.cursor()
c.execute("SELECT * FROM HalUri")

array_1 = []
for uri in c :
    array_1.append(uri[0])

index_publications(array_1)
