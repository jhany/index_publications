import datetime
import sqlite3

i = datetime.datetime.now()

con = sqlite3.connect('date_and_pub_uri.db')
c = con.cursor()

c.execute("DROP TABLE if exists Date_update")
con.commit()

c.execute("CREATE TABLE Date_update (date text)")
con.commit()

c.execute("""INSERT INTO Date_update(date) VALUES (?);""", (i.isoformat(),))
con.commit()
