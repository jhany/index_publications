import sqlite3
import requests
import json

con = sqlite3.connect('uri.db')
c = con.cursor()

c.execute("DROP TABLE if exists HalUri")
con.commit()

c.execute("CREATE TABLE HalUri (uri text)")
con.commit()

resp = requests.get('http://api.archives-ouvertes.fr/search/inria/?q=structure_t:inria')
nbpub_json = json.loads(resp.text)
nbpub = nbpub_json["response"]["numFound"]
nbpub = int(nbpub/1000) + 1

for i in range(0, nbpub) :
    r = requests.get('http://api.archives-ouvertes.fr/search/inria/?q=structure_t:inria&start='+str(i*1000)+'&rows=1000')
    convert_json = json.loads(r.text)
    for docu in  convert_json["response"]["docs"] :
        c.execute("""INSERT INTO HalUri(uri) VALUES (?);""", (docu['uri_s'],))
    con.commit()
